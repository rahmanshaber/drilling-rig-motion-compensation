# drilling-rig motion compensation


## Info
These  heave compensation mechanisms uses 3 cylinder to the motion compensation while drilling in a drilling-rig in sea.

* Motion compensation 1 have heave, surge and sway compensation.
* Motion compensation 2 have roll, pitch and heave compensation.

<img src="qq.png" width="500">
<img src="ww.png" width="500">

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

If you want to license them for commercial purpose contact me via email, rahmanshaber@yahoo.com. Other then that above license is applied. I also want to know if you are using these mechanisms, email me.
